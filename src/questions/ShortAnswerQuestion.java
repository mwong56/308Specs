package questions;

/**
 * A short answer question has a string shortAnswer for the student's short
 * answer and the test-maker's answer notes.
 */
public abstract class ShortAnswerQuestion extends Question {
  String shortAnswer;
  String answerNotes;
}
