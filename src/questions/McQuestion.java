package questions;

import java.util.Collection;

/**
 * A McQuestion has a collection of choices and a choice that the student
 * chooses.
 */
public abstract class McQuestion extends Question {
  Collection<String> choices;
  String studentChoice;
  String correctChoice;
}
