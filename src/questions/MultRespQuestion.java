package questions;

import java.util.Collection;

/**
 * A multiple response question has a collection of strings that the student
 * inputs as their answers.
 */
public abstract class MultRespQuestion extends Question {
  Collection<String> studentAnswers;
  Collection<String> correctAnswers;
}
