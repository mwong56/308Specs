package questions;

import java.util.Collection;
import java.util.Map;

/**
 * A Matching question has a collection of keys and values. It also has a
 * collection of maps, mapping keys and values for the students answers.
 */
public abstract class MatchingQuestion extends Question {
  Collection<String> key;
  Collection<String> value;

  Collection<Map<String, String>> studentAnswers;
  Collection<Map<String, String>> correctAnswers;
}
