package questions;

import java.util.Collection;

/**
 * A Fill in question has an array of answers that the student needs to fill in
 * for.
 */
public abstract class FillInQuestion extends Question {
  Collection<String> studentAnswers;
  Collection<String> correctAnswers;
}
