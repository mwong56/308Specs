package admin;

import java.util.Collection;

/**
  * Student class that stores the student's name and id.
  */
abstract class Student {
  Collection<Course> courses;
  Name name;
  long id;
}

/**
  * Houses backend data of student screen.
  */
abstract class AdminStudent {
  Collection<Course> allCourses;
  Collection<Student> allStudents;

  /**
    //
    // A student is in the output data (section.students) if it is
    // the new student to be added or it is in the input data.
    // A course is in the output data (student.courses) if it is
    // the new course to be added or it is in the input data.
    //
    post:
      forall(Course course_other ;
        (data'.contains(course_other)) if
          course_other.equals(course) || data.contains(course_other))

  abstract void deleteStudent(Student student, Course course);
              &&

      forall(Student student_other ;
        (data'.contains(student_other)) if
          student_other.equals(student) || data.contains(student_other));
  *
  */
  abstract void addStudent(Student student, Course course, Section section);

  /**
    //
    // There is at least one student in allStudents.
    // There is at least one course in allCourses.
    // The student to be deleted is in allStudents.
    // The course that the student is enrolled in is in allCourses.
    //
    pre:
      !allCourses.isEmpty() && !allStudents.isEmpty()

              &&

      allCourses.contains(course) && allStudents.contains(student);
      //
      // A student is not in the output data (section.students) if it is the
      // student to be deleted or it is in the input data.
      // A course is not in the output data (student.courses) if it is the
      // course that the student to be deleted is enrolled in or it is in
      // the input data.
      //
    post:
      forall(Course course_other ;
        (!data'.contains(course_other)) if
          course_other.equals(course) || data.contains(course_other))

              &&

      forall(Student student_other ;
        (!data'.contains(student_other)) if
          student_other.equals(student) || data.contains(student_other));
  *
  */
  abstract void deleteStudent(Student student, Course course, Section section);
}

abstract class Course {
  Collection<Section> sections;
  String name;
}

abstract class Section {
  Collection<Student> students;
  int secNum;
}
