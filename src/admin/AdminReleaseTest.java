package admin;


/**
  * In Class test
  */
abstract class InClass {
  boolean closedBook;
}

/**
  * Practice test class
  */
abstract class Practice {
  boolean closedBook;
  int numTries;
}

/**
  * Time class for take home test
  */
abstract class Time {
    int hour;
    int min;
    boolean am;
}

/**
 * Take Home test
 */
abstract class TakeHome {
  Time startTime;
  Time endTime;
  boolean closedBook;
  int numTries;
}

/**
  * Release Test screen
  */
abstract class AdminReleaseTest {
  //Collection<Test> tests;

  /**
    //
    // Use In Class class to save settings to selected test
    //
    pre:
      //
      // There is at least one test in the test database to be released as an
      // in-class test.
      //
      !tests.isEmpty() && tests.contains(test_other);
    post:
      //
      // A test is in the output data if and only if it is the new test to be
      // added or it is in the input data.
      //
      forall(Test test_other ;
        (data'.contains(test_other)) iff
          test_other.equals(test) || data.contains(test_other));
  *
  */
  abstract void isInClass();

  /**
    //
    // Use Practice class to save settings to selected test
    //
    pre:
      //
      // There is at least one test in the test database to be released as an
      // in-class test.
      //
      !tests.isEmpty() && tests.contains(test_other);
    post:
      //
      // A test is in the output data if and only if it is the new test to be
      // added or it is in the input data.
      //
      forall(Test test_other ;
        (data'.contains(test_other)) iff
          test_other.equals(test) || data.contains(test_other));
  *
  */
  abstract void isPractice();

  /**
    //
    // Use TakeHome class to save settings to selected test
    //
    pre:
      //
      // There is at least one test in the test database to be released as an
      // in-class test.
      //
      !tests.isEmpty() && tests.contains(test_other);
    post:
      //
      // A test is in the output data if and only if it is the new test to be
      // added or it is in the input data.
      //
      forall(Test test_other ;
        (data'.contains(test_other)) iff
          test_other.equals(test) || data.contains(test_other));
  *
  */
  abstract void isTakeHome();

  //abstract void releaseTest(Test test);
}
