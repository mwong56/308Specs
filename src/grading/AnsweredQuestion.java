package grading;

/**
 * A single question from a student's completed test. Contains a reference to
 * the question at hand, as well as info regarding its current grading status.
 * 
 * @author (Casey Sheehan)
 * @version (Milestone 6)
 */
abstract class AnsweredQuestion {
  // Question question; //from Question.java elsewhere
  boolean reviewNeeded; // flag to indicate whether professor needs to examine
                        // the current question

}
