package grading;

import java.util.Collection;

/**
 * an individual test representation that has been completed by a student.
 * Contains a list of answered questions, as well as any pertinent information
 * about the test -- i.e. scores and such.
 * 
 * @author (Casey Sheehan)
 * @version (Milestone 6)
 */
abstract class StudentTakenTest {
  Collection<AnsweredQuestion> questions;
  double score;
  String status;
}
