package grading;

import java.util.Collection;

/**
 * Lists all of the tests for which the given professor has access. Also allows
 * the teacher to access the list of students for each of these tests.
 * 
 * @author (Casey Sheehan)
 * @version (Milestone 6)
 */
abstract class ProfessorTestList {
  Collection<TakenTest> testList;
}
