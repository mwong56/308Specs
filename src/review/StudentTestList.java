package review;
import java.util.Collection;
/**
 * A list of the tests that a single user (in this case, a student) has completed. This class differentiates itself from the
 * CompletedTest class in that it is only associated with a single student.
 * 
 * @author (Casey Sheehan) 
 * @version (Milestone 6)
 */
abstract class StudentTestList
{
    Collection<StudentCompletedTest> studentTests;
}
