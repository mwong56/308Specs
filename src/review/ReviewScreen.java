package review;
//import home.NavBar;
/**
 * The Review Screen for the TestTool displays the review view, which is used by STUDENT users to review tests which they themselves have completed.
 * As of Milestone 6, this screen is now separate from that of a professor's, which is now considered a "GradingScreen".
 * 
 * @author (Casey Sheehan) 
 * @version (Milestone 6)
 */
public abstract class ReviewScreen
{
    StudentTestList studentsList;
}
