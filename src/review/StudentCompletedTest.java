package review;
import java.util.Collection;
/**
 * an individual test representation that has been completed by a student. Contains a list of graded questions, 
 * as well as any pertinent information about the test -- i.e. scores and such.
 * 
 * @author (Casey Sheehan) 
 * @version (Milestone 6)
 */
public abstract class StudentCompletedTest
{
    Collection<GradedQuestion> questions;
    double score;
    String status;
}
