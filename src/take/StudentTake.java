package take;

import proctor.StudentProctor;
import review.StudentCompletedTest;

/**
 * The StudentTakeTest class is the interface where the students will actually
 * be taking the test.
 */
public abstract class StudentTake extends StudentProctor{
  Test test;
  
  
  /*
   * pre:
   *  //
   *  //
   *  //
   * post:
   *  //
   */
  abstract AnswerQuestion answerQuestion();

  /*
   * 
   */
  abstract StudentAsk askInstructor();
  
  /*
   * pre:
      time != null;
      
    post:
      time' == time - 1;
   */
  abstract void countDown();

  /*
   * pre: 
   *  // There exists a test
   *  
   */
  abstract StudentCompletedTest submitTest();
}
