package take;

import java.util.*;

/**
 * The student tests simply contains a list of tests the student can choose from.
 */
public abstract class StudentTests {
  Collection<Test> tests;
  Test test;

  /*
   * 
   *  pre:
   *    //
   *    // The test exists and isn't null
   *    //
   *    test != null;
   * 
   */
  abstract Test selectTest();
}
