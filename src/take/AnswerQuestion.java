package take;

import questions.Question;


/**
 * The AnswerQuestion class is the interface where students will be answering
 * any of the eight types of questions
 *
 */
public abstract class AnswerQuestion {
	Question question;
	
	/*
	 * Answers the question
	 * 
	 *<pre>
	 * pre:
	 *   //
	 *   // There is a question to answer.
	 *   //
	 *   question != null;
	 * post:
	 *   //
	 *   // The question will have been started to be answered
	 *   //
	 *
	 */
	abstract void answerQuestion(Question question);
}