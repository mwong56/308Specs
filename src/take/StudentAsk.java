package take;

import proctor.StudentQuestion;

/**
 * The StudentAsk class is the interface where the student asks the instructor
 * a question.
 *
 */
abstract class StudentAsk {
	Student student;

	/*
	 * This method returns a student's question to be read by the proctor.
	 *
	 * <pre>
	 *  pre: 
	 *  //
	 *  // The student exists
	 *  //
	 *  student != null;
	 *  post:
	 *  //
	 *  // The professor will have the student's question in the database
	 *  //
	 *  
	 *
	 */
	abstract StudentQuestion askQuestion();
}
