package create;

import questions.QuestionBank;

/**
 * View of the Test with the questions
 */
public abstract class ManualView {
	Test currentTest;
	
	/**
	 * @param QuestionBank
	 * 
	 pre: 
	 // QuestionBank has to exist
	 QuestionBank.contains(Question);
	 
	 post:
	 // add the Question to currentTest
	 forall(Test test_other ;
        (data'.contains(test_other)) iff
          test_other.equals(test) || data.contains(test_other));
	 */
	abstract void addQuestion(QuestionBank a);
}
