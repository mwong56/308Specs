package create;

import java.util.Collection;

/**
 * Contains a history of all the created tests in the database of the teacher
 */
public abstract class HistoryView {
	
	Collection<Test> createdTests;
	
	/**
<<<<<<< HEAD
	 * 
	 pre: 
	 // Test has to exist in the createdTests database
	 createdTests != null && createdTests.contains(t);
	  
	 post:
	 // ManualView has to be created or opened
	 forall(Test test_other ;
=======
	 * @return ManualView
	 * 
	 * pre: 
	 * Test has to exist in the createdTests database
	 * createdTests.contains(t);
	 * 
	 * post:
	 * ManualView has to be created or opened
	 * exists(ManualView);
	 * &&
	 * forall(Test test_other ;
>>>>>>> 436dfd0c13c8e2b5b2f1530a31b475a98f3f35b6
        (createdTests'.contains(test_other)) iff
          test_other.equals(test) || createdTests.contains(test_other));
	 */
	abstract ManualView editTest(Test t);
	
	/**
	 * @TestSettingView
	 * 
<<<<<<< HEAD
	 pre:
	 // Test has to exist in the createdTests database
	 createdTests.contains(t);
	 
	 post:
	 // Opens up TestSettingView
	 forall(Test test_other ;
=======
	 * pre:
	 * Test has to exist in the createdTests database
	 * createdTests.contains(t);
	 * 
	 * post:
	 * Opens up TestSettingView
	 * exists(TestSettingView);
	 * &&
	 * forall(Test test_other ;
>>>>>>> 436dfd0c13c8e2b5b2f1530a31b475a98f3f35b6
        (createdTests'.contains(test_other)) iff
          test_other.equals(test) || createdTests.contains(test_other));
	 */
	abstract TestSettingView editSettings();
}
