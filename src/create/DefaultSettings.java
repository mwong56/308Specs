package create;

/*
 * Default settings of the tests
 */
public abstract class DefaultSettings {
	double setTime;
	int currDifficulty;
	double diffRange;
	int numMC;
	int numTF;
	int numFill;
	int numCode;
	String resources;
}
