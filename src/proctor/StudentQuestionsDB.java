package proctor;

import java.util.Collection;

/**
 * The student question db holds a list of questions and allows users to add,
 * update, or get a question.
 */
public abstract class StudentQuestionsDB {
  Collection<StudentQuestion> questions;

  /**
   * addQuestion(StudentQuestion question) adds the input question into
   * this.questions;
   * 
   * @param question
   *          Takes in a question which will be added to this.questions;
   * 
   *          pre: (question != null && question.prompt.length() > 0 &&
   *          question.id >= 0) post: this.questions.contains(question)
   */
  abstract void addQuestion(StudentQuestion question);

  /**
   * updateQuestion(StudentQuestion question) updates the db with the input
   * question.
   * 
   * @param question
   *          Takes in a question which will be added to
   * 
   *          pre: (question != null && question.prompt.length() > 0 &&
   *          question.id >= 0 && this.questions.contains(question)) post:
   *          !this.questions.contains(question)
   */
  abstract void updateQuestion(StudentQuestion question);

  /**
   * getQuestion(int id) queries the db with the input id.
   * 
   * 
   * @param id
   *          Id of the student question to retrieve.
   * @return the StudentQuestion corresponding to the param id.
   * 
   *         pre: (id >= 0 && this.db.contains(id))
   * 
   */
  abstract StudentQuestion getQuestion(int id);

  /**
   * Retrieves the list of StudentQuestions asked by input student.
   * 
   * @param student
   *          The student to retrieve the questions for.
   * @return A list of StudentQuestions.
   */
  abstract Collection<StudentQuestion> getQuestions(StudentProctor student);
}