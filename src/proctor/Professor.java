package proctor;

import java.util.Collection;

/**
 * The professor class contains a list of student questions. This list will be
 * generated from the StudentQuestionDB. When the professor chooses to edit or
 * answer a question, he/she will simply edit directly from the studentQuestions
 * collection object. To save a question, the question will call the save method
 * from the db.
 */
public abstract class Professor {
  Collection<StudentQuestion> questions;
  StudentQuestionsDB db;

  /**
   * getQuestions() will call this.db to populate this.questions;
   * pre: 
   *    (db != null && db.size() > 0)
   *    
   * post:
   *    (questions != null && questions.size() > 0)
   *    
   */
  abstract void getQuestions();

  /**
   * saveQuestions() will call this.db to save this.questions;
   * pre:
   *    (questions != null && questions.size() > 0)
   *    
   * post:
   *      (db != null && db.size() > 0)
   */
  abstract void saveQuestions();
}
