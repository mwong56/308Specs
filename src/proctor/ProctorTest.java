package proctor;

/**
 * The ProctorTest class determines whether the user is a student or a professor.
 */
public abstract class ProctorTest {
  Professor professor;
  StudentProctor student;
  boolean isStudent;
}
